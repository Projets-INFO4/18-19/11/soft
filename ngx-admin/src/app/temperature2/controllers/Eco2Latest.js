'use strict';

var utils = require('../utils/writer.js');
var Eco2Latest = require('../service/Eco2LatestService');

module.exports.eco2Latest = function eco2Latest (req, res, next) {
  Eco2Latest.eco2Latest(res);
};
