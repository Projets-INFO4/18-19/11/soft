'use strict';

var utils = require('../utils/writer.js');
var No2Latest = require('../service/No2LatestService');

module.exports.no2Latest = function no2Latest (req, res, next) {
  No2Latest.no2Latest(res);
};
