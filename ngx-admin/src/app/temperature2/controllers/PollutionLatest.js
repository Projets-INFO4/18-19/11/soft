'use strict';

var utils = require('../utils/writer.js');
var PollutionLatest = require('../service/PollutionLatestService');

module.exports.pollutionLatest = function pollutionLatest (req, res, next) {
  PollutionLatest.pollutionLatest(res);
};
