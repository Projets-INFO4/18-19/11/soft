'use strict';

var utils = require('../utils/writer.js');
var Pm10Latest = require('../service/Pm10LatestService');

module.exports.pm10Latest = function pm10Latest (req, res, next) {
  Pm10Latest.pm10Latest(res);
};
