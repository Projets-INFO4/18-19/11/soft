'use strict';

var utils = require('../utils/writer.js');
var TemperatureWeek = require('../service/TemperatureWeekService');

module.exports.temperatureWeek = function temperatureWeek (req, res, next) {
  TemperatureWeek.temperatureWeek(res);
};
