'use strict';

var utils = require('../utils/writer.js');
var TimeLatest = require('../service/TimeLatestService');

module.exports.timeLatest = function timeLatest (req, res, next) {
  TimeLatest.timeLatest(res);
};
