'use strict';

var utils = require('../utils/writer.js');
var O3Latest = require('../service/O3LatestService');

module.exports.o3Latest = function o3Latest (req, res, next) {
  O3Latest.o3Latest(res);
};
