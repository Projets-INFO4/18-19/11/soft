'use strict';

var utils = require('../utils/writer.js');
var TemperatureLatest = require('../service/TemperatureLatestService');

module.exports.temperatureLatest = function temperatureLatest (req, res, next) {
  TemperatureLatest.temperatureLatest(res);
};
