'use strict';

var utils = require('../utils/writer.js');
var LonLatest = require('../service/LonLatestService');

module.exports.lonLatest = function lonLatest (req, res, next) {
  LonLatest.lonLatest(res);
};
