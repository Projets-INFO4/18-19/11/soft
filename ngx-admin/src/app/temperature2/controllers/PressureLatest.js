'use strict';

var utils = require('../utils/writer.js');
var PressureLatest = require('../service/PressureLatestService');

module.exports.pressureLatest = function pressureLatest (req, res, next) {
  PressureLatest.pressureLatest(res);
};
