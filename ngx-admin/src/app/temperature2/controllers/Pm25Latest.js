'use strict';

var utils = require('../utils/writer.js');
var Pm25Latest = require('../service/Pm25LatestService');

module.exports.pm25Latest = function pm25Latest (req, res, next) {
  Pm25Latest.pm25Latest(res);
};
