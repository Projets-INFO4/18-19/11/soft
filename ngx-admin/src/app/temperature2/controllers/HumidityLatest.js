'use strict';

var utils = require('../utils/writer.js');
var HumidityLatest = require('../service/HumidityLatestService');

module.exports.humidityLatest = function humidityLatest (req, res, next) {
  HumidityLatest.humidityLatest(res);
};
