'use strict';


/**
 * Get the latest no2 available in the db
 * Get the latest no2 available in the db
 *
 * returns no2
 **/
exports.no2Latest = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432
  });
  client.connect();
  client.query('SELECT no2 FROM mesures WHERE no2 IS NOT NULL order by temps desc', (err, data) => {
    if (err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(JSON.stringify(data.rows[0]["no2"]));
    client.end();
  });
}

