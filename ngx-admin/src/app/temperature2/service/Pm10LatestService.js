'use strict';


/**
 * Get the latest PM10 available in the db
 * Get the latest PM10 available in the db
 *
 * returns pm10
 **/
exports.pm10Latest = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432
  });
  client.connect();
  client.query('SELECT pm10 FROM mesures WHERE pm10 IS NOT NULL order by temps desc', (err, data) => {
    if (err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(JSON.stringify(data.rows[0]["pm10"]));
    client.end();
  });
}

