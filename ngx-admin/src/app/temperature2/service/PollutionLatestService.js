'use strict';


/**
 * Get the latest pollution available in the db
 * Get the latest pollution available in the db
 *
 * returns pollution
 **/
exports.pollutionLatest = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432,
  });
  client.connect();
  
  client.query('SELECT o3 FROM mesures WHERE o3 IS NOT NULL order by temps desc', (err, data) => {
    if (err) throw (err);
    var o3, pm10, no2;
    o3 = data.rows[0]["o3"];
    client.query('SELECT pm10 FROM mesures WHERE pm10 IS NOT NULL order by temps desc', (err, data) => {
      if (err) throw (err);
      pm10 = data.rows[0]["pm10"];
      console.log(data.rows);
      client.query('SELECT no2 FROM mesures WHERE no2 IS NOT NULL order by temps desc', (err, data) => {
        if (err) throw (err);
        no2 = data.rows[0]["no2"];
        var io3 = o3<29? 1: o3<54? 2: o3<79? 3: o3<104? 4: o3<129? 5: o3<149? 6: o3<179? 7: o3<209? 8: o3<239? 9: 10;
        var ipm10 = pm10<6? 1: pm10<13? 2: pm10<20? 3: pm10<27? 4: pm10<34? 5: pm10<41? 6: pm10<49? 7: pm10<64? 8: pm10<79? 9: 10;
        var ino2 = no2<29? 1: no2<54? 2: no2<84? 3: no2<109? 4: no2<134? 5: no2<164? 6: no2<199? 7: no2<274? 8: no2<399? 9: 10;
        console.log("Indice = " + Math.max(io3,ipm10,ino2));
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.end(JSON.stringify(Math.max(io3, ipm10, ino2)));
        client.end();
      });
    });
  });


  
}

