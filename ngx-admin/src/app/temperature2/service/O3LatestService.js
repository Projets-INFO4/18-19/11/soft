'use strict';


/**
 * Get the latest o3 available in the db
 * Get the latest o3 available in the db
 *
 * returns o3
 **/
exports.o3Latest = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432
  });
  client.connect();
  client.query('SELECT o3 FROM mesures WHERE o3 IS NOT NULL order by temps desc', (err, data) => {
    if (err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(JSON.stringify(data.rows[0]["o3"]));
    client.end();
  });
}

