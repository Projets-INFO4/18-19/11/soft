'use strict';


/**
 * Get the average temperature for the 7 preceding days
 * Get the average temperature for the 7 preceding days
 *
 * returns temperatureWeek
 **/
exports.temperatureWeek = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432
  });
  client.connect();
  client.query('SELECT date_trunc(\'day\', temps) as \"Day\", AVG(temperature) as \"avgTemp\" from mesures group by 1 order by 1 desc;', (err, data) => {
    if (err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    var rslt =[]
    for(var i=0; i<8; i++){
      rslt.push(Number(data.rows[i]["avgTemp"]).toPrecision(3));
    }
    res.end(JSON.stringify(rslt));
    client.end();
  });
}

