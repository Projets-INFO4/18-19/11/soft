'use strict';


/**
 * Get the latest lat available in the db
 * Get the latest lat available in the db
 *
 * returns lat
 **/
exports.latLatest = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432
  });
  client.connect();
  client.query('SELECT lat FROM arbre WHERE lat IS NOT NULL order by temps desc', (err, data) => {
    if (err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(JSON.stringify(data.rows[0]["lat"]));
    client.end();
  });
}

