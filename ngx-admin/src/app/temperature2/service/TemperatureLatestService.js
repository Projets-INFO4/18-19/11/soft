'use strict';


/**
 * Get the latest temperature available in the db
 * Get the latest temperature available in the db
 *
 * returns temperature
 **/
exports.temperatureLatest = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432
  });
  client.connect();
  client.query('SELECT temperature FROM mesures WHERE temperature IS NOT NULL order by temps desc', (err, data) => {
    if (err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(JSON.stringify(data.rows[0]["temperature"]));
    client.end();
  });
}

