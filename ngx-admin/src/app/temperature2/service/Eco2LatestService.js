'use strict';


/**
 * Get the latest eco2 available in the db
 * Get the latest eco2 available in the db
 *
 * returns eco2
 **/
exports.eco2Latest = function(res) {
  var pg = require('pg');
  const client = new pg.Client({
    user: 'test',
    host: '',
    database: 'mabase',
    password: 'test',
    port: 5432
  });
  client.connect();
  client.query('SELECT eco2 FROM mesures WHERE eco2 IS NOT NULL order by temps desc', (err, data) => {
    if (err) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(JSON.stringify(data.rows[0]["eco2"]));
    client.end();
  });
}

