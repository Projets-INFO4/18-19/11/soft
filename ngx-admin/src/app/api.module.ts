import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { Eco2LatestService } from './api/eco2Latest.service';
import { HumidityLatestService } from './api/humidityLatest.service';
import { IdLatestService } from './api/idLatest.service';
import { LatLatestService } from './api/latLatest.service';
import { LonLatestService } from './api/lonLatest.service';
import { No2LatestService } from './api/no2Latest.service';
import { O3LatestService } from './api/o3Latest.service';
import { Pm10LatestService } from './api/pm10Latest.service';
import { Pm25LatestService } from './api/pm25Latest.service';
import { PollutionLatestService } from './api/pollutionLatest.service';
import { PressureLatestService } from './api/pressureLatest.service';
import { TemperatureLatestService } from './api/temperatureLatest.service';
import { TemperatureWeekService } from './api/temperatureWeek.service';
import { TimeLatestService } from './api/timeLatest.service';
import { VocLatestService } from './api/vocLatest.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    Eco2LatestService,
    HumidityLatestService,
    IdLatestService,
    LatLatestService,
    LonLatestService,
    No2LatestService,
    O3LatestService,
    Pm10LatestService,
    Pm25LatestService,
    PollutionLatestService,
    PressureLatestService,
    TemperatureLatestService,
    TemperatureWeekService,
    TimeLatestService,
    VocLatestService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
