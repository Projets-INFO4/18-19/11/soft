/**
 * Swagger Mesures
 * This is a sample server temperature server. For this sample, you can use the api key `special-key` to test the authorization filters.
 *
 * OpenAPI spec version: 1.1.0
 * Contact: ergi.sala@etu.univ-grenoble-alpes.fr
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export type Lon = number;
