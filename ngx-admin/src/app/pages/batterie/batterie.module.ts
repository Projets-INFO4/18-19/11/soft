import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { SolarComponent } from '../dashboard/solar/solar.component';
import { BatterieComponent } from './batterie.component';
import { PortComponent } from './port/port.component';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
  ],
  declarations: [
    BatterieComponent,
    PortComponent,
    SolarComponent,
  ],
})
export class BatterieModule { }
