import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { QualityComponent } from './quality.component';
import { ECommerceProgressSectionComponent } from '../e-commerce/progress-section/progress-section.component';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
  ],
  declarations: [
    QualityComponent,
    ECommerceProgressSectionComponent,
  ],
})
export class QualityModule { }
