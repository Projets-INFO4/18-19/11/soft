import { Component, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { TemperatureWeek } from '../../../model/models';
import { TemperatureWeekService } from '../../../api/api';
//import { getTemplateContent } from '@angular/core/src/sanitization/html_sanitizer';

@Component({
  selector: 'ngx-echarts-multiple-xaxis',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsMultipleXaxisComponent implements /*AfterViewInit*/ OnDestroy, OnInit {
  options: any = {};
  themeSubscription: any;
  temperatures : TemperatureWeek;

  constructor(private theme: NbThemeService, private tempService : TemperatureWeekService) {
    this.temperatures = [];
  }

  ngOnInit() {
    this.tempService.temperatureWeek().subscribe(data => {
      this.temperatures = data;
      this.treatement();
    });
  }

  treatement() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      this.options = {
        backgroundColor: echarts.bg,
        color: colors.success,
        tooltip: {
          trigger: 'none',
          axisPointer: {
            type: 'cross',
          },
        },
        legend: {
          data: 'Température de la semaine',
          textStyle: {
            color: echarts.textColor,
          },
        },
        grid: {
          top: 70,
          bottom: 50,
        },
        xAxis:
          {
            type: 'category',
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              onZero: false,
              lineStyle: {
                color: colors.info,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
            data: [
              'Jour J-7',
              'Jour J-6',
              'Jour J-5',
              'Jour J-4',
              'Jour J-3',
              'Jour J-2',
              'Jour J-1',
              'Jour J',
            ],
          },
        yAxis:
          {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        series:
          [{
            name: 'Température de la semaine',
            type: 'line',
            smooth: true,
            data: this.temperatures,
          }],
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
