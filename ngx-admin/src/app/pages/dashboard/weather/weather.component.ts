import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { registerLocaleData } from '@angular/common';

import localeFr from '@angular/common/locales/fr';
import { Pressure, Temperature, Humidity } from '../../../model/models';
import { PressureLatestService, TemperatureLatestService, HumidityLatestService } from '../../../api/api';

registerLocaleData(localeFr);

@Component({
  selector: 'ngx-weather',
  styleUrls: ['./weather.component.scss'],
  templateUrl: './weather.component.html',
})

export class WeatherComponent implements OnInit {
  
  temperature: Observable<Temperature>;
  humidity : Observable<Humidity>;
  pressure : Observable<Pressure>
  date = new Date();
  

  constructor(private temperatureService: TemperatureLatestService,private humidityService : HumidityLatestService,
     private pressureService : PressureLatestService){
    this.temperature = of();
    this.humidity = of();
    this.pressure = of();
    formatDate(this.date,'EEEE, MMMM d, y','fr-FR');
  }

  ngOnInit(){
    this.temperature = this.temperatureService.temperatureLatest();
    this.humidity = this.humidityService.humidityLatest();
    this.pressure = this.pressureService.pressureLatest();
  }
}
