import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProgressInfo, StatsProgressBarData } from '../../../@core/data/stats-progress-bar';
import { takeWhile } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Pollution, Pm10, Pm25, No2, O3, Voc, Eco2 } from '../../../model/models';
import { PollutionLatestService, Pm10LatestService, Pm25LatestService, No2LatestService, O3LatestService, VocLatestService, Eco2LatestService } from '../../../api/api';

@Component({
  selector: 'ngx-progress-section',
  styleUrls: ['./progress-section.component.scss'],
  templateUrl: './progress-section.component.html',
})
export class ECommerceProgressSectionComponent implements OnInit {

  private alive = true;
  pollution : Observable<Pollution>;
  description : string;
  pm10 : Observable<Pm10>
  pm25 : Observable<Pm25>
  no2 : Observable<No2>
  o3 : Observable<O3>
  voc : Observable<Voc>
  eco2 : Observable<Eco2>


  constructor(private pollutionService : PollutionLatestService, private pm10Service : Pm10LatestService, private pm25Service : Pm25LatestService,
    private no2Service : No2LatestService, private o3Service : O3LatestService, private vocService : VocLatestService, private eco2Service : Eco2LatestService) {
      this.pollution = of();
      this.pm25 = of();
      this.pm10 = of();
      this.o3 = of();
      this.voc = of();
      this.no2 = of();
      this.eco2 = of();
      this.description = '';
  }

  ngOnInit() {
    this.pollution = this.pollutionService.pollutionLatest();
    this.pollution.subscribe(value => {
      this.description = this.setStatus(value)
    });
    this.no2 = this.no2Service.no2Latest();
    this.o3 = this.o3Service.o3Latest();
    this.pm25 = this.pm25Service.pm25Latest();
    this.pm10 = this.pm10Service.pm10Latest();
    this.voc = this.vocService.vocLatest();
    this.eco2 = this.eco2Service.eco2Latest();
  }

  private setStatus(value : number) : string {
      if(value < 2){
        return 'Très Bon';
      } else if (value < 4){
        return 'Bon';
      } else if (value < 6){
        return 'Moyen';
      } else if (value < 8){
        return 'Médiocre';
      } else{
        return 'Mauvais';
      }
  }
}
