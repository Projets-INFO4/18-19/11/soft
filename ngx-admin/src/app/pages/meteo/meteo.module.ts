import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
//import { DashboardComponent } from '../dashboard/dashboard.component';
import { TemperatureComponent } from '../dashboard/temperature/temperature.component';
import { TemperatureDraggerComponent } from '../dashboard/temperature/temperature-dragger/temperature-dragger.component';
import { WeatherComponent } from '../dashboard/weather/weather.component';
import { EchartsMultipleXaxisComponent } from '../charts/echarts/echarts-multiple-xaxis.component';
import { MeteoComponent } from './meteo.component';


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
  ],
  declarations: [
    WeatherComponent,
    TemperatureDraggerComponent,
    TemperatureComponent,
    MeteoComponent,
    EchartsMultipleXaxisComponent,
  ],
})
export class MeteoModule { }
